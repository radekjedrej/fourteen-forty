<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- ios -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

	<!-- icon in the highest resolution we need it for -->
	<link rel="icon" sizes="192x192" href="/assets/img/favicons/favicon-192x192.png" />

	<!-- Favicon 32x32px -->
	<link rel="shortcut icon" sizes="32x32" href="/assets/img/favicons/favicon-32x32.png" />

	<!-- Multiple icons for Safari -->
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/img/favicons/favicon-76x76.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/img/favicons/favicon-128x128.png" />
	<link rel="apple-touch-icon" href="/assets/img/favicons/favicon-192x192.png" />

	<!-- Browser theme colour schema -->
	<meta name="theme-color" content="#ffffff">

	<?php include 'css-enqueue.php'; ?>

</head>
