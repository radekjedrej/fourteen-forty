<section class="fullwidthSlider__block">

	<!-- TODO: Conditional to not display this block if not exists slides -->
	<div class="fullwidthSlider__slider">

		<!-- Repeater code -->

		<!-- TODO: Replace background-image url with the image URL field -->
		<div class="slide" style="background-image: url('./assets/img/content/slide-example.jpg')">
			<div class="inner--slide">
				<div class="section__constrained" data-bottom-top="transform: translateY(100px)" data-top-bottom="transform: translateY(-100px)" >

					<!-- TODO: Repalce h1 tag content with Title text field -->
					<h1 data-aos="fade-left" data-aos-duration="500">Call to action text here</h1>

					<!-- TODO: Repalce p tag content with Description text field. Not render if the field is empty -->
					<p data-aos="fade-left" data-aos-delay="50" data-aos-duration="500">Desccription text. normally for banners without buttons</p>

					<!-- TODO: Conditional to not display this block if fields button 1 and button 2 are empty -->
					<ul>
						<!-- TODO: Conditional to not display this block if not exists button 1 -->
						<li data-aos="fade-left" data-aos-delay="100" data-aos-duration="500">
							<!-- TODO: Repalce href and content of a tag with button 1 field -->
							<a href="#" class="btn">Example button</a>
						</li>

						<!-- TODO: Conditional to not display this block if not exists button 2 -->
						<li data-aos="fade-left" data-aos-delay="150" data-aos-duration="500">
							<!-- TODO: Repalce href and content of a tag with button 2 field -->
							<a href="#" class="btn secondary">Secondary button</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<!-- End of repeater code -->

	</div>

</section>