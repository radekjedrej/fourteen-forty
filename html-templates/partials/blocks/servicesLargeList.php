<section class="services-lg-spacing section__constrained">
	<h2 class="heading-2">The best quality integrated facilities management service</h2>
	<p class="services__intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisinulla pariatur.</p>

	<ul class="pure-g">
		<li class="pure-u-1 pure-u-lg-1-2 services-border">
			<a href="#" class="services-lg">
				<img class="b-lazy services__icon--lg" src="./assets/img/icons/14forty-icon-Security.svg" data-src="./assets/img/icons/14forty-icon-Security.svg" alt="">
				<div class="services__info">
					<h3 class="heading-3">Service name here</h3>
					<p class="services__copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
			</a>
		</li>

		<li class="pure-u-1 pure-u-lg-1-2 services-border">
			<a href="#" class="services-lg">
				<img class="b-lazy services__icon--lg" src="./assets/img/icons/14forty-icon-Security.svg" data-src="./assets/img/icons/14forty-icon-Security.svg" alt="">
				<div class="services__info">
					<h3 class="heading-3">Service name here</h3>
					<p class="services__copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
			</a>
		</li>

		<li class="pure-u-1 pure-u-lg-1-2 services-border">
			<a href="#" class="services-lg">
				<img class="b-lazy services__icon--lg" src="./assets/img/icons/14forty-icon-Security.svg" data-src="./assets/img/icons/14forty-icon-Security.svg" alt="">
				<div class="services__info">
					<h3 class="heading-3">Service name here</h3>
					<p class="services__copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
			</a>
		</li>

		<li class="pure-u-1 pure-u-lg-1-2 services-border">
			<a href="#" class="services-lg">
				<img class="b-lazy services__icon--lg" src="./assets/img/icons/14forty-icon-Security.svg" data-src="./assets/img/icons/14forty-icon-Security.svg" alt="">
				<div class="services__info">
					<h3 class="heading-3">Service name here</h3>
					<p class="services__copy">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
			</a>
		</li>
		
	</ul>
</section>