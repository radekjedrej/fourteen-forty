<section id="homepage--banner" class="background__green-gradient spaceOnTop">
	<div class="section__constrained">

		<div class="pure-g">
			<div class="pure-u-1 pure-u-lg-2-3 pure-u-xl-1-2">
				<div class="banner--content">
					<h1>Facilities management services every minute of the day</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
						labore et dolore
						magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
						ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
						fugiat nulla
						pariatur.</p>
					<a href="/" class="btn spaceOnTop">CTA Button</a>
				</div>
			</div>
			<div id="image--wrapper" class="pure-u-1 pure-u-lg-1-3 pure-u-xl-1-2">
				<img class="b-lazy"
					 src="/assets/img/content/homepage-header.png"
					 data-src="/assets/img/content/homepage-header.png">
			</div>
		</div>

		<div class="bottom--space"></div>
	</div>
</section>