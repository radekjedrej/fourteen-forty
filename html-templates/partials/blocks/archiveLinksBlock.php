<section class="archiveLinks__block">

	<!-- TODO: Conditional to not display this block if not exists links inside the repeater -->

	<!-- TODO: Add class modifier to "wrapper__padding" class attribute to set the option selected from field Block spacing -->
	<div class="wrapper__padding">
		<div class="section__constrained center">

			<!-- TODO: Replace h2 tag content with Block Title text field. Not render if the field is empty -->
			<h2 class="h3" data-aos="fade" data-aos-duration="500">Our venues</h2>

			<div class="negative--margin">
				<div class="pure-g horizontal--scroll">

					<!-- TODO: Create a variable that will increase 100 for each loop iteration starting fron 0 and add this value to attribute data-aos-delay. Below php implementation for guidance and delay brake by row -->

					<!--
					php incremental example, check end of loop:
					$item = 0;
					$delay = 0;
					for ($i = 0; $i < 3; $i++) {
					-->

					<?php
					$item = 0;
					$delay = 0;
					for ($i = 0; $i < 8; $i++) { ?>

					<!-- Repeater code -->

					<div class="pure-u-1 pure-u-lg-1-3 pure-u-xl-1-4" data-aos="fade-up"
						 data-aos-delay="<?php echo $delay; ?>">
						<div class="link__container cta"
							 data-aos-duration="500">
							<div class="image__wrapper">

								<!-- TODO: Repalce img tag src attribute with image field URL and alt attribute with Title field + icon. Not render if the field is empty -->
								<img class="b-lazy" src="./assets/img/content/slide-example.jpg" data-src="./assets/img/content/slide-example.jpg" alt="Christmas Parties">

							</div>
							<div class="content__wrapper">
								<a href="#">
									<!-- TODO: Replace h4 tag content with Title text field. Not render if the field is empty -->
									<h4>Platinum Suite</h4>
								</a>
							</div>
						</div>
					</div>

					<!--
					php incremental example and delay reset by row, end of loop:
					$item = $item + 1;
						$delay = $delay + 100;

						if ($item == 4) {
							$item = 0;
							$delay = 0;
						}
					-->

					<!-- End of repeater code -->

					<?php
						$item = $item + 1;
						$delay = $delay + 100;

						if ($item == 4) {
							$item = 0;
							$delay = 0;
						}
					} ?>

					<div class="h--separator"><p></p></div>
				</div>
			</div>

		</div>
	</div>

</section>