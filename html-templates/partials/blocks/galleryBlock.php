<section class="gallery__block">
	<div class="wrapper__padding">
		<div class="section__constrained center">

			<h2 class="h3 aos-init aos-animate" data-aos="fade" data-aos-duration="500">Gallery</h2>

			<div data-aos="fade" data-aos-delay="0" data-aos-duration="700">
				<div class="gallery-slider">
					<div class="gallery-slide">
						<img src="./assets/img/content/slide-example.jpg">
					</div>
					<div class="gallery-slide">
						<img src="./assets/img/content/slide-example.jpg">
					</div>
					<div class="gallery-slide">
						<img src="./assets/img/content/slide-example.jpg">
					</div>
					<div class="gallery-slide">
						<img src="./assets/img/content/slide-example.jpg">
					</div>
					<div class="gallery-slide">
						<img src="./assets/img/content/slide-example.jpg">
					</div>

				</div>
			</div>
			<div data-aos="fade" data-aos-delay="100" data-aos-duration="700">
				<div class="gallery-slider-nav__wrapper">
					<div class="gallery-slider-nav">
						<div class="gallery-slide">
							<img src="./assets/img/content/slide-example.jpg">
						</div>
						<div class="gallery-slide">
							<img src="./assets/img/content/slide-example.jpg">
						</div>
						<div class="gallery-slide">
							<img src="./assets/img/content/slide-example.jpg">
						</div>
						<div class="gallery-slide">
							<img src="./assets/img/content/slide-example.jpg">
						</div>
						<div class="gallery-slide">
							<img src="./assets/img/content/slide-example.jpg">
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>