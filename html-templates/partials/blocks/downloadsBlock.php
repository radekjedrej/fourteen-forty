<!-- TODO: Conditional to not display if Title field is empty -->
<section class="downloads__block">

	<div class="wrapper__padding double">

		<div class="section__constrained center" data-bottom-top="transform: translateY(75px)"
			 data-top-bottom="transform: translateY(-75px)">

			<!-- TODO: Repalce h3 tag content with Title text field -->
			<h3 class="h1" data-aos="fade-left" data-aos-duration="500">Packages & Menus</h3>

			<div class="negative--margin">
				<div class="pure-g horizontal--scroll">

					<!-- TODO: Create a variable that will increase 100 for each loop iteration starting fron 0 and add this value to attribute data-aos-delay. Below php implementation for guidance and delay brake by row -->

					<!--
					php incremental example, check end of loop:
					$item = 0;
					$delay = 0;
					for ($i = 0; $i < 3; $i++) {
					-->

					<?php
					$item = 0;
					$delay = 0;
					for ($i = 0; $i < 8; $i++) { ?>

						<!-- Repeater code -->

						<div class="pure-u-1 pure-u-lg-1-3 pure-u-xl-1-4" data-aos="fade-up"
							 data-aos-delay="<?php echo $delay; ?>">
							<div class="link__container cta"
								 data-aos-duration="500">
								<div class="image__wrapper">
									<svg xmlns="http://www.w3.org/2000/svg" width="105.788" height="138.424"
										 viewBox="-262.395 1.021 105.788 138.424">
										<g>
											<path d="M-158.681 28.263l-25.168-25.168a.136.136 0 0 1-.138-.132v-.006a7.505 7.505 0 0 0-4.979-1.936h-65.686a7.73 7.73 0 0 0-7.744 7.718v122.963a7.73 7.73 0 0 0 7.718 7.743h90.326a7.73 7.73 0 0 0 7.744-7.718V33.379a8.459 8.459 0 0 0-2.073-5.116zm-26.135-20.052l20.881 20.881h-19.359a1.387 1.387 0 0 1-1.383-1.383V8.211h-.139zm23.921 123.351a3.424 3.424 0 0 1-3.389 3.459H-254.651a3.455 3.455 0 0 1-3.6-3.457V8.765a3.66 3.66 0 0 1 3.6-3.6h65.686v22.4a5.608 5.608 0 0 0 5.545 5.67h22.525v98.327z"/>
											<path class="hover" d="M-218.559 71.131h-.553v3.457h.553c1.245 0 2.627 0 2.627-1.658.001-1.799-1.243-1.799-2.627-1.799zM-206.25 71.27h-.83v7.744h.83c2.073 0 4.01-1.106 4.01-3.872a3.668 3.668 0 0 0-4.01-3.872z"/>
											<path class="hover" d="M-173.063 60.622h-72.046a3.717 3.717 0 0 0-3.734 3.702v21.189a3.716 3.716 0 0 0 3.702 3.733h72.078a3.717 3.717 0 0 0 3.734-3.701v-21.19a3.719 3.719 0 0 0-3.734-3.733zm-53.931 21.71a2.073 2.073 0 1 1 2.074-2.074 1.957 1.957 0 0 1-2.074 2.074zm9.957-4.84h-2.075v4.563h-3.6v-13.69h5.531c2.9 0 4.978 1.384 4.978 4.563.006 3.044-1.792 4.564-4.834 4.564zm11.478 4.564h-5.117v-13.69h5.117a6.849 6.849 0 1 1 0 13.69zm16.594-10.786h-4.287v2.352h3.873v3.043h-3.873v5.393h-3.591V68.365h7.882l-.004 2.905z"/>
										</g>
									</svg>
								</div>
								<div class="content__wrapper">
									<!-- TODO: Replace href attribute with the file URL -->
									<a href="#">
										<!-- TODO: Replace h4 tag content with Title text field. Not render if the field is empty -->
										<h4>Day Delegate Package</h4>
									</a>
								</div>
							</div>
						</div>

						<!--
						php incremental example and delay reset by row, end of loop:
						$item = $item + 1;
							$delay = $delay + 100;

							if ($item == 4) {
								$item = 0;
								$delay = 0;
							}
						-->

						<!-- End of repeater code -->

						<?php
						$item = $item + 1;
						$delay = $delay + 100;

						if ($item == 4) {
							$item = 0;
							$delay = 0;
						}
					} ?>

					<div class="h--separator"><p></p></div>
				</div>
			</div>

		</div>

	</div>

</section>