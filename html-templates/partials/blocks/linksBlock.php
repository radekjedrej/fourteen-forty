<section class="links__block">

	<!-- TODO: Conditional to not display this block if not exists links inside the repeater -->

	<!-- TODO: Add class modifier to "wrapper__padding" class attribute to set the option selected from field Block spacing -->
	<div class="wrapper__padding">
		<div class="section__constrained center">

			<!-- TODO: Replace h2 tag content with Block Title text field. Not render if the field is empty -->
			<h2 class="h3" data-aos="fade" data-aos-duration="500">What's New</h2>

			<div class="negative--margin">
				<div class="pure-g horizontal--scroll">

					<!-- TODO: Create a variable that will increase 100 for each loop iteration starting fron 0 and add this value to attribute data-aos-delay. Below php implementation for guidance -->

					<!--
					php incremental example, check end of loop:
					$delay = 0;
					for ($i = 0; $i < 3; $i++) {
					-->

					<?php
					$delay = 0;
					for ($i = 0; $i < 3; $i++) { ?>

					<!-- Repeater code -->

					<div class="pure-u-1 pure-u-md-1-3" data-aos="fade-up"
						 data-aos-delay="<?php echo $delay; ?>">
						<div class="link__container cta wrapper__flex center--align center--justify"
							 data-aos-duration="500">
							<div class="content__wrapper">

								<!-- TODO: Repalce img tag src attribute with image field URL and alt attribute with Title field + icon. Not render if the field is empty -->
								<img class="b-lazy" src="./assets/img/icons/snowflake.svg" data-src="./assets/img/icons/snowflake.svg" alt="Christmas Parties icon">

								<!-- TODO: Replace h4 tag content with Title text field. Not render if the field is empty -->
								<h4>Christmas Parties</h4>
							</div>
							<div class="button__wrapper">

								<!-- TODO: Repalce href and content of a tag with button field -->
								<a class="btn secondary noSpacing" href="#">Find Out more</a>
							</div>
						</div>
					</div>

					<!--
					php incremental example, end of loop:
					$delay = $delay + 100;
					-->

					<!-- End of repeater code -->

					<?php
						$delay = $delay + 100;
					} ?>

					<div class="h--separator"><p></p></div>
				</div>
			</div>

		</div>
	</div>

</section>