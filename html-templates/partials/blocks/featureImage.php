<section class="backgorund__pale-green feature-image">
    <div class="section__constrained">
        <div class="pure-g feature-image__text--wrapper">
            <div class="pure-u-1-1 pure-u-md-3-5 feature-image__text">
                <h2 class="h1--beta colour-text-body">Management every minute of the day</h2>
                <h3 class="colour-text-body">Title text here Proxima Nova</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                <a href="/" class="btn spaceOnTop">CTA Button</a>
            </div>
        </div>
    </div>
    <div class="feature-image__background"></div>
</section>