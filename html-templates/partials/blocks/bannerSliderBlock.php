<section class="bannerSlider__block">

	<!-- TODO: Conditional to not display this block if not exists slides -->
	<div class="bannerSlider__slider">

		<!-- Repeater code -->

		<div class="slide">
			<div class="inner--slide">
				<div class="section__constrained">

					<!-- TODO: Repalce h1 tag content with Title text field -->
					<blockquote class="wrapper__max width--1000" data-aos="fade-left" data-aos-duration="500">
						<p>The Royal Berkshire Conference Centre was a great choice for our recent exhibition. The venue
							was great and all the staff were incredibly professional and helpful in the run up to the
							event and on the day of the exhibition, nothing was too much trouble. The venue was great
							and I would definitely recommend it to others.</p>
						<footer> Erin Bray, Marketing and Events Officer, Radian</footer>
					</blockquote>

				</div>
			</div>
		</div>

		<!-- End of repeater code -->

	</div>

</section>