<!-- TODO: Conditional to not display if Title field is empty -->
<section class="singleImage__block">

	<div class="wrapper__padding double">

		<div class="section__constrained center" data-bottom-top="transform: translateY(75px)" data-top-bottom="transform: translateY(-75px)" >

			<!-- TODO: Repalce h1 tag content with Title text field -->
			<h3 data-aos="fade-left" data-aos-duration="500">Silver Suite Plant</h3>

			<!-- TODO: Repalce "content--wrapper" content content with Description text field -->
			<div class="content--wrapper wrapper__max width--600 white--content" data-aos="fade-left" data-aos-delay="50" data-aos-duration="500">
				<p>Talk to our team for more information on how we can accommodate for your event.</p>
			</div>

			<!-- TODO: Replace href and content of a tag with button field. Not render the div wrapper if button is empty -->
			<div class="image--wrapper" data-aos="fade-left" data-aos-delay="100" data-aos-duration="500">
				<img class="b-lazy" src="/assets/img/content/sc_platplan.png" data-src="/assets/img/content/sc_platplan.png">
			</div>

		</div>

	</div>

</section>