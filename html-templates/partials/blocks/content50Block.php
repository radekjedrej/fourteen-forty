<!-- TODO: Add class modifier to "content50__block" class attribute to set the option selected from field Content position (normal/reverse) -->
<section class="content50__block normal">

	<!-- TODO: Add class modifier to "wrapper__padding" class attribute to set the option selected from field Block spacing -->

	<div class="section__constrained">
		<div class="pure-g">
			<div class="pure-u-1 pure-u-lg-1-2">

			</div>
			<div class="pure-u-1 pure-u-lg-1-2">
				<div class="content__container wrapper__padding">

					<!-- TODO: Repalce i tag content with Subheading text field. Not render if the field is empty -->
					<i data-aos="fade-left" data-aos-duration="500">Reading CF</i>

					<!-- TODO: Repalce h2 tag content with Heading text field. Not render if the field is empty -->
					<h2 class="h3" data-aos="fade-left" data-aos-delay="50" data-aos-duration="500">Welcome to the Royal</h2>


					<!-- TODO: Repalce "regular--content" content with Description text field. Not render if the field is empty -->
					<div class="regular--content wrapper__text" data-aos="fade-left" data-aos-delay="150" data-aos-duration="500">
						<p>
							The largest conference facility in the Thames Valley, the Royal Berkshire Conference Centre offers a variety of flexible event space for any conference or event and our flexible approach and passionate team will ensure your event becomes an experience.
						</p>
					</div>

					<!-- TODO: Repalce href and content of a tag with button field. Not render the div wrapper if button is empty -->
					<div data-aos="fade-left" data-aos-delay="200" data-aos-duration="500">
						<a href="#" class="btn">Read more</a>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="wrapper__image b-lazy mask" data-src="./assets/img/content/slide-example.jpg"></div>

</section>