<section class="contact__block background__grey">

	<!-- TODO: Add class modifier to "wrapper__padding" class attribute to set the option selected from field Block spacing -->
	<div class="wrapper__padding">
		<div class="section__constrained">

			<!-- TODO: Replace h2 tag content with Title text field. Not render if is empty -->
			<h2 data-aos="fade-left" data-aos-duration="500">Get in touch</h2>

			<div class="address--wrapper" data-aos="fade-left" data-aos-delay="50" data-aos-duration="500">
				<ul>
					<li>
						<!-- TODO: Replace a tag content with Phone text field. Not render if is empty -->
						<a href="tel:0118 968 1333">0118 968 1333</a>
					</li>
					<li>
						<!-- TODO: Replace address tag content with Address text field. Not render if is empty -->
						<address>Royal Berkshire Conference Centre, Reading, RG2 0FL</address>
					</li>
					<li>
						<!-- TODO: Replace a tag content with Email text field. Not render if is empty -->
						<a href="mailto:rbcc.events@compass-group.co.uk">rbcc.events@compass-group.co.uk</a>
					</li>
				</ul>
			</div>

			<div class="negative--margin">
				<div class="pure-g">

					<div class="pure-u-1 pure-u-md-1-2">
						<div class="column--spacer">
							<div class="contact--form--wrapper" data-aos="fade-left" data-aos-delay="100" data-aos-duration="500">

								<!-- TODO: Render Form picker content. The code below is just for visual purpose, will be replaced by the Umbraco form -->
								<div id="umbraco_form_2595d9a1d30b44d9afcfdc6c86492730" class="umbraco-forms-form contact umbraco-forms-">

									<form action="/" enctype="multipart/form-data" method="post" _lpchecked="1"><input name="__RequestVerificationToken" type="hidden" value="j9Y8nlpae4ZCr8nR2HEHv07tmz3WyC8XzG0uGERxQBajbVwRLbJwuLXAZJaXgHvXct4MVbkyLf5nBz26f0GXaVqA_nt9jaKJU330t7VEqck1"><input data-val="true" data-val-required="The FormId field is required." id="FormId" name="FormId" type="hidden" value="2595d9a1-d30b-44d9-afcf-dc6c86492730"><input id="FormName" name="FormName" type="hidden" value="Contact"><input data-val="true" data-val-required="The RecordId field is required." id="RecordId" name="RecordId" type="hidden" value="00000000-0000-0000-0000-000000000000"><input id="PreviousClicked" name="PreviousClicked" type="hidden" value="">            <input type="hidden" name="FormStep" value="0">
										<input type="hidden" name="RecordState" value="QTcwRjEzNUZEQkI1MUU0RENFNEMzQjFCNjQ2RjkwOUY4MTU1NUE3Q0JGNjA0NjJERDg0NDQ3NzZCMjg5MUE5NERGNDNFMjM0OTQ1MTM0Mzk5MDY1QTNFQjdGQ0VCOEY2QURCMUQxODA4NjUxNzYxRjNGQjk0Qjc1MTcxQ0I3MkQ3MDJCQTU5NTJDMUUyRUNFOEE3RDA5MTA4RDA3QThEMDA0MUMxNTcxREZEMUMzNEUwQUUxMUUzRDY3NTlFODJDMzg3NzQ5NjI5N0Y4OEYxNDQxNjExQzEyNTE5RkUzNzIxMkY1ODE4QUIxQzhDOTBCMTJCRENBMjMyMjQ4MkU1MjE4MzhBNENEMzdCQzA3OTJEMTQ5MkJGOENCNTQwM0EyNDA3RjgyMEFBOEZCMzdBOUY5QUNFQ0RGRjI4QzcyQ0MyREI5OTJFQkQ5MTI1REQ4MjkxOURGQUNBRDg0MDkyQUQ4OEVDRDZERkYzQzI1MkQzRTE1QzM5OTc0RkM4MkJENDdGN0Y5OTBEMkZBNUVEOEVEQTczOTFGRTU1QUE4QTQyRjFEMUQ0MTJBQ0IwQ0ZBNzE1RjAyQkUyNDQ3ODY0NDVEMUFDOUQwNTk4NDMxOTg1MTM5M0FCNTEzNTU3MTg5MzUwMzJENTMwQzJBMjcyRjIzQzQ5NjQ1NDE1NzY0MUJEMDM0Qzk1MjkzODBGRUM3MDRFMTM2MUNDNTkzNERGNzZGQTBDNkUxMTdBNzVDRDk1Mzk5N0M3MDZGMjE5NDU0MUNBQjg3MzE2N0I5QTc3RUQ3Q0IwNUY0QkFCMTMyRDM0MjFEQTYzMDkzRTZCMzdDOEI0QjVFQjJGNUI4MzRFN0REQkM4MzdFMTU4QkNFOURCRkVEMTc3OERENjNDRUFBMEQ0QTVBRTU3OTRFNTdFMTE0MkM2Q0Y0QzU4OEY5RTJFREFBOUFCREIwQjUyN0EzMDBCRTZEMjQxMjE3MUZCMzlDODY2MThDM0M0ODc2NTU1NTk5NDk1QUQ2Mzc1OUUzMjJENjBGNTFGQjFBQTdDQTg1OTU0MkVCMjc2MTEyMkM5RjFDQzdDNTgyOEJERTY3M0FENUVBNjUyMDVBNEVBMUZEREE3MjUxRTc1NTNBRjlGNzIzMjdGOUM0NEU3RjY2MDRGQTBEMDUxRUE5OEVBNjMzNDJEODJBM0IyQzI2RTM4RjVERjE0NDk2QTgyODQ3MTkzMUQ5M0FEQjVBOEQyQUREQzA4MTFDOUEwMjZBOTA5QTM2NzM1QzcxRkIxN0QxMjdFNzYyNzg2MzNDMTQ1RUE1OTNENDlBMDJCNEFFRjAxODU0MTU5NDEyQjUwRTYyRTNERDFDMUQ0MUVDNENFMEZGQkREQjE3MjgyQzBCQkQ3QzQ0NThGNzE3MzUyMDc5NkNFNTEyMzgxNTZCNzlBQzIxM0JDNzMxQzkxOTcxMUM1QUI3MDdDRkJFOTQ2RERBRkM2Njc5MTBDRUI2QTIzODc4MzA2QTFBNDU2QTQ1RENCQjNERkE1Njk1MzQ1RTJFMzdCMkM2MTdBQjM0NTBFNjIwM0M0MDg5OTM0RURBMEJBMjIwNkRCODJFOTJBOTgzMzgyMzdDMDBGNUFENUVFMEQxMzA0NkI4ODc1OUZCRTJBOTM5MTI0NTc2RjA3OUI3NDNCQUFEREY2OUMxQTcyMkQyRjE2NzdCQ0Y5OTYyMkEzNjFBRTU5NDNGNDE0MDU0QkIzREZBMDEwNjEzMjU0MzRBNTREQzYzNDhBMUU5NkY4MENFNDU3OUYwNUNENjI1RjhBNjNCQkMzNjUxMUQwRTM0Rjg2NUE3QkZGNkY0M0Q1M0VBNDBENzRFN0ZCRDU0QjRBMkEzQTg5MTNDMzYxQUE0ODAyRUNEMDVENzIzNDAwMEQ2MzgyQjZBRUMwMjc4RjI5RDg4MDNDOTUzREMzN0I1Q0Q2QzYzOTcxRTAyMzk3MTg4N0Y4NkQzREE2MjFBMTYzQTMyQkY3RDYzQTE3QzM0MzcyNjU2NkZENzM3NjJCNkZFN0VGN0EyNzdGQjJENDMwOTY2NUFFOUY4NUJEMTU3MEZFNTQ2QTlDRkEzQkYyRDBEMTgzRjdDQzQ4QzZCNDQxNUY4MTRGQ0JFMzgyREVCM0Q3NzRFREY4QjU2MTNFNjBBOEI1NDg5OEFEQ0EyOTJEOTAzOEMyQjYzQTg4RTRBOTRCQjVDQkFFQUQwMDkzQUQ3NDhDMjM3RTBDRUEwOTcxMjgzMTM5QjFCNjdBMTkwQjMzQjk1MzQ4RkFBMjZEREE2RkRFQzVBQ0JFMTEwQjJEMDEwQTQzQkI3NDZFREM4NjFCNUFFRDVEODgyNDU1NUJCMThEN0RBMzE0MkY3N0Q2NEY2QUI3NDQ2QTI5QjBCRkMyMTdFOUMwOUMzQjE2M0M1NDJBNEQyNTU3M0MwQ0VDMjEwMEQ3QUM0NEFCNDIyRTY5OEFGNEUwOUFGMTA3RTBERTdGQ0UwNUUxQTVEMDhGQzlEMzA3MkEwNEJBMjAzRUJBQjkzODQyMUYxODA2MDk5RDEzRDQzMTEyOTM0MzU1NTM1ODk5RDEzRjM4MjYwMjYyQkFCQzJFOTIxNkNGNzQ4QjZFNjNENENGOTdEM0JGNUQyMzBEMDAwQjE0MTNFQTkwMjMyQTJBNTVFRjNFNDI1NDJGOUUzRDY2QzRBQTZGRTU1NkQ2NjkzNUNFOUVGOTI1NjYzMTQ2NzU0RTRENjNFODg4QTQzQ0JCN0M5NENCMENBRTI0Q0M2QzlFODA4MDk3ODM5MzEwODY0MUMxRDhGNTI0NDZBODY4MzVDOUY5Q0VENTAxQ0JDODVFQUMxQzBGNjc3REFBMTk0Mjg5RDI3ODYyMjhCMzYzRTc5RjkwRkE3Q0IxQzY5QUI3MA0KOEQyRTRENTg1RDI3MUJBQ0E1M0FEOEU4OTFBOEU5RDUxQzMzQTE4MzVERUE1OUE5MDIxNzZEQjdDMkZEMzlBQkQ3QTg3QzI5MzMyMEI5OENGODAwNEZGOUVEMUQzMEQzNTZGRUZBMDNDRkMyRkVFMDA5MUZERUQ2N0VCQzhCRjA4NTI0N0FEQ0I0NDA4RDc3MjJCREJFQ0I0RUEzNTQ0NDcwODE2ODEzMzMwMTEzNzk1NTlBREYyN0YzRkY4RkE1OEVGMDYwRDEzODNGQ0IwRkE4QUE1ODgwNjRFRDJDMUJCNENEQUUyRTMyNzEwMUJDMzUxQkQ3OTVDRTA2OUU5NjE3RTE0MzRCRTkxOEMyRTkyQkY5NkNGNUQ4NjFDMTYzNDBDOTE2ODExRDUzNjgxOEI0NUNBMDNEQzc0RjYwOEE=">





										<div class="umbraco-forms-page">




											<fieldset class="umbraco-forms-fieldset" id="9d580641-76b9-4e79-bbd8-64427a6561ba">


												<div class="row-fluid">

													<div class="umbraco-forms-container col-md-12">

														<div class=" umbraco-forms-field intro titleanddescription">



															<div class="umbraco-forms-field-wrapper">




																<div id="fa047cae-0247-4ea6-9201-41f6324ebcd0" class="">
																	<h2>Please feel free to contact us with any questions or queries you may have.</h2>
																</div>

																<span class="field-validation-valid" data-valmsg-for="fa047cae-0247-4ea6-9201-41f6324ebcd0" data-valmsg-replace="true"></span>
															</div>

														</div>
														<div class=" umbraco-forms-field name shortanswer mandatory alternating">

															<label for="d84dd300-18db-437e-c33c-43a746c1c4a1" class="umbraco-forms-label">
																Name                                             <span class="umbraco-forms-indicator">*</span>
															</label>


															<div class="umbraco-forms-field-wrapper">


																<input type="text" name="d84dd300-18db-437e-c33c-43a746c1c4a1" id="d84dd300-18db-437e-c33c-43a746c1c4a1" class=" text" value="" maxlength="500" data-val="true" data-val-required="Please provide a value for Name" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">





																<span class="field-validation-error" data-valmsg-for="d84dd300-18db-437e-c33c-43a746c1c4a1" data-valmsg-replace="true">Please provide a value for Name</span>
															</div>

														</div>
														<div class=" umbraco-forms-field email shortanswer mandatory">

															<label for="402d5500-fe6b-4473-aa35-8a09a13a6386" class="umbraco-forms-label">
																Email                                             <span class="umbraco-forms-indicator">*</span>
															</label>


															<div class="umbraco-forms-field-wrapper">


																<input type="text" name="402d5500-fe6b-4473-aa35-8a09a13a6386" id="402d5500-fe6b-4473-aa35-8a09a13a6386" class=" text" value="" maxlength="500" data-val="true" data-val-required="Please provide a value for Email" data-val-regex="Please provide a valid value for Email" data-regex="[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+">





																<span class="field-validation-valid" data-valmsg-for="402d5500-fe6b-4473-aa35-8a09a13a6386" data-valmsg-replace="true"></span>
															</div>

														</div>
														<div class=" umbraco-forms-field phone shortanswer alternating">

															<label for="826f92b5-6d0e-4e40-c4c9-f857d8b9064a" class="umbraco-forms-label">
																Phone                                     </label>


															<div class="umbraco-forms-field-wrapper">


																<input placeholder="Phone number" type="text" name="826f92b5-6d0e-4e40-c4c9-f857d8b9064a" id="826f92b5-6d0e-4e40-c4c9-f857d8b9064a" class=" text" value="" maxlength="500">





																<span class="field-validation-valid" data-valmsg-for="826f92b5-6d0e-4e40-c4c9-f857d8b9064a" data-valmsg-replace="true"></span>
															</div>

														</div>

													</div>
												</div>

											</fieldset>

											<fieldset class="umbraco-forms-fieldset" id="8a5701cf-4599-49cc-91ce-e05a334e2adc">


												<div class="row-fluid">

													<div class="umbraco-forms-container col-md-12">

														<div class=" umbraco-forms-field enquiry longanswer alternating">

															<label for="de0084f8-a702-4442-9e7d-b79ab60ea348" class="umbraco-forms-label">Enquiry</label>


															<div class="umbraco-forms-field-wrapper">


																<textarea class="" name="de0084f8-a702-4442-9e7d-b79ab60ea348" id="de0084f8-a702-4442-9e7d-b79ab60ea348" rows="2" cols="20"></textarea>



																<span class="field-validation-valid" data-valmsg-for="de0084f8-a702-4442-9e7d-b79ab60ea348" data-valmsg-replace="true"></span>
															</div>

														</div>

													</div>
												</div>

											</fieldset>
											<fieldset class="umbraco-forms-fieldset" id="056c2e91-454d-4353-c660-4b69b716403d">


												<div class="row-fluid">

													<div class="umbraco-forms-container col-md-12">

														<div class=" umbraco-forms-field privacypolicy checkbox mandatory alternating">

															<label for="aa3c07c1-9f51-43c2-f473-aee74fe3fa84" class="umbraco-forms-label">
																Privacy Policy                                             <span class="umbraco-forms-indicator">*</span>
															</label>


															<div class="umbraco-forms-field-wrapper">


																<input type="checkbox" name="aa3c07c1-9f51-43c2-f473-aee74fe3fa84" id="aa3c07c1-9f51-43c2-f473-aee74fe3fa84" class="" data-val="true" data-val-requiredcb="Please provide a value for Privacy Policy">

																<span class="field-validation-error" data-valmsg-for="aa3c07c1-9f51-43c2-f473-aee74fe3fa84" data-valmsg-replace="true"></span>
															</div>

														</div>
														<div class=" umbraco-forms-field privacypolicytext titleanddescription">



															<div class="umbraco-forms-field-wrapper">




																<div id="56ac91d4-ce38-4425-ef47-55dc500b5026" class="">
																	<h2>Tick here to agree to our terms and conditions and privacy policy</h2>
																</div>

																<span class="field-validation-valid" data-valmsg-for="56ac91d4-ce38-4425-ef47-55dc500b5026" data-valmsg-replace="true"></span>
															</div>

														</div>


													</div>
												</div>

											</fieldset>

											<div style="display: none">
												<input type="text" name="2595d9a1d30b44d9afcfdc6c86492730">
											</div>


											<div class="umbraco-forms-navigation row-fluid">

												<div class="col-md-12">
													<input type="submit" class="btn primary" value="Submit" name="__next">
												</div>
											</div>
										</div><input name="ufprt" type="hidden" value="E2177213EB6329A044C326129ABE7E747BC8F3977FEBCE09DCD1C4AA5FDD6B93E1401A14F8D4B6C97CB36B2CD5AF02E1BD646F1B199706C6938E906F36A78C0797D6C6CA8F9490224EB8166E28C8FDC318C29CB891031DA1F2686B58DE1D201BBC1E437A98F6E1B80544436B99AC94BA9EF813FF93568CD2CEDDEA36670F24BB24BA94A9F3D87ADAA0DB9D4E87FF244B"></form>

									<script>
																	if (typeof umbracoFormsCollection === 'undefined') var umbracoFormsCollection = [];
																	umbracoFormsCollection.push({"formId":"2595d9a1d30b44d9afcfdc6c86492730","fieldSetConditions":{},"fieldConditions":{},"recordValues":{}});
									</script>


									<script src="/App_Plugins/UmbracoForms/Assets/Themes/Default/umbracoforms.js" defer="defer"></script><link rel="stylesheet" href="/App_Plugins/UmbracoForms/Assets/Themes/Default/style.css">    </div>
							</div>
						</div>
					</div>

					<!-- TODO: Conditional to not display this block if More information, Longitude, Latitude fields are empty -->
					<div class="pure-u-1 pure-u-md-1-2">
						<div class="column--spacer">

							<!-- TODO: Conditional to not display this block if Longitude or Latitude fields are empty -->
							<div id="gmap" data-aos="fade-left" data-aos-delay="150" data-aos-duration="500"></div>

							<div class="description--wrapper" data-aos="fade-left" data-aos-delay="200" data-aos-duration="500">
								<p>
									<strong>Directions by road From</strong><br> Junction 11 of the M4, follow signs towards Reading. Madejski Stadium is on your left-hand side after less than a mile. For SatNav systems, our postcode is RG2 0FL. Complimentary parking is available onsite on a non-match day.
								</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<!-- TODO: Conditional to not display this block if Longitude or Latitude fields are empty -->

<!-- TODO: Replace lng value with Longitude field and lat value with Latitude field -->
<script>
	// Initialize and add the map
	function initMap() {
		var uluru = {lat: 51.760642058942175, lng: -0.20929813385009766};
		// The map, centered at Uluru
		var map = new google.maps.Map(
			document.getElementById('gmap'), {zoom: 15, center: uluru, disableDefaultUI: true});
		// The marker, positioned at Uluru
		var marker = new google.maps.Marker({position: uluru, map: map});
	}
</script>
<!--Load the API from the specified URL
* The async attribute allows the browser to render the page while the API loads
* The key parameter will contain your own API key (which is not needed for this tutorial)
* The callback parameter executes the initMap() function
-->
<!-- TODO: Conditional to not display this block if Longitude or Latitude fields are empty -->
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoWg7hLJxk_83LHhFl6tWKZ7UKjxttz3M&callback=initMap"></script>