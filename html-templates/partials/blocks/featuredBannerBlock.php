<!-- TODO: Conditional to not display if Title field is empty -->
<section class="featuredBanner__block">

	<div class="wrapper__padding double">

		<div class="section__constrained center" data-bottom-top="transform: translateY(75px)" data-top-bottom="transform: translateY(-75px)" >

			<!-- TODO: Repalce h1 tag content with Title text field -->
			<h1 data-aos="fade-left" data-aos-duration="500">Check our offers</h1>

			<!-- TODO: Repalce "content--wrapper" content content with Description text field -->
			<div class="content--wrapper wrapper__max width--600 white--content" data-aos="fade-left" data-aos-delay="50" data-aos-duration="500">
				<p>Talk to our team for more information on how we can accommodate for your event.</p>
			</div>

			<!-- TODO: Replace href and content of a tag with button field. Not render the div wrapper if button is empty -->
			<div class="button--wrapper" data-aos="fade-left" data-aos-delay="100" data-aos-duration="500">

				<!-- TODO: Add to a tag class "secondary" if Button style field is marked as "Secondary button" -->
				<a href="#" class="btn">View offers</a>
			</div>

		</div>

	</div>

	<!-- TODO: Add to div tag class "wrapper__overlay" option selected from Image overlay. Not render if Image field is empty-->
	<div class="wrapper__overlay cardiff" data-aos="fade" data-aos-delay="100" data-aos-duration="500"></div>

	<!-- TODO: Replace data-src attribute with the image URL field. Not render this block if Image is empty -->
	<div class="wrapper__parallax" data-bottom-top="transform: translateY(35px)" data-top-bottom="transform: translateY(-35px)" >
		<div class="wrapper__image b-lazy mask" data-src="./assets/img/content/slide-example.jpg"></div>
	</div>


</section>