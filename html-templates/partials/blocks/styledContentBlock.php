<section class="styledContent__block">

	<!-- TODO: Add class modifier to "wrapper__padding" class attribute to set the option selected from field Block spacing -->
	<div class="wrapper__padding">
		<div class="section__constrained">

			<!-- TODO: Repalce i tag content with Subheading text field. Not render if the field is empty -->
			<i data-aos="fade-left" data-aos-duration="500">Reading CF</i>

			<!-- TODO: Repalce h2 tag content with Heading text field. Not render if the field is empty -->
			<h2 class="h3" data-aos="fade-left" data-aos-delay="50" data-aos-duration="500">Welcome to the Royal</h2>
			<div class="content__container" data-aos="border-top" data-aos-delay="100" data-aos-duration="500">

				<!-- TODO: Repalce em tag content with Featured text field. Not render if the field is empty -->
				<em data-aos="fade-left" data-os-delay="100" data-aos-duration="500">
					The Royal Berkshire Conference Centre, based in the impressive Madejski Stadium complex, provides the perfect unusual backdrop to any conference or event. The modern facilities, combined with fantastic catering, award winning service and outstanding views make the Royal Berkshire Conference Centre the ideal venue for any conference, meeting, exhibition or dinner.
				</em>

				<!-- TODO: Repalce "regular--content" content with Description text field. Not render if the field is empty -->
				<!-- TODO: Add class modifier to "regular--content" class attribute to set columns--2 if field Number of content columns is set to 2 -->
				<div class="regular--content columns--2" data-aos="fade-left" data-aos-delay="150" data-aos-duration="500">
					<p>
						Positioned just outside of Reading town centre, along the M4 corridor, just over two miles from Reading train station, with excellent transport links to London and beyond, the Royal Berkshire Conference Centre offers the ideal location for any delegate or guest. The venue has over 2,000 complimentary car parking spaces meaning we can accommodate every delegate and is just twenty miles from London Heathrow.
					</p>
					<p>
						The largest conference facility in the Thames Valley, the Royal Berkshire Conference Centre offers a variety of flexible event space for any conference or event and our flexible approach and passionate team will ensure your event becomes an experience.
					</p>

				</div>

				<!-- TODO: Repalce href and content of a tag with button field. Not render the div wrapper if button is empty -->
				<div data-aos="fade-left" data-aos-delay="200" data-aos-duration="500">
					<a href="#" class="btn">Read more</a>
				</div>

			</div>
		</div>
	</div>

</section>