<section class="services-sm services-sm--padding">
	<div class="section__constrained">
		<ul class="services__wrapper">
			<li class="services">
				<div class="services__inner">
					<img class="b-lazy services__icon" src="./assets/img/icons/14forty-icon-Business.svg" data-src="./assets/img/icons/14forty-icon-Business.svg" alt="">
					<p class="services__text">Food and hospitality</p>
				</div>
			</li>
			<li class="services">
				<div class="services__inner">
					<img class="b-lazy services__icon" src="./assets/img/icons/14forty-icon-cleaning.svg" data-src="./assets/img/icons/14forty-icon-cleaning.svg" alt="">
					<p class="services__text">Cleaning</p>
				</div>
			</li>
			<li class="services">
				<div class="services__inner">
					<img class="b-lazy services__icon" src="./assets/img/icons/14forty-icon-Business.svg" data-src="./assets/img/icons/14forty-icon-Business.svg" alt="">
					<p class="services__text">Food and hospitality</p>
				</div>
			</li>
			<li class="services">
				<div class="services__inner">
					<img class="b-lazy services__icon" src="./assets/img/icons/14forty-icon-Business.svg" data-src="./assets/img/icons/14forty-icon-Business.svg" alt="">
					<p class="services__text">Food and hospitality</p>
				</div>
			</li>
			<li class="services">
				<div class="services__inner">
					<img class="b-lazy services__icon" src="./assets/img/icons/14forty-icon-Business.svg" data-src="./assets/img/icons/14forty-icon-Business.svg" alt="">
					<p class="services__text">Food and hospitality</p>
				</div>
			</li>
			<li class="services">
				<div class="services__inner">
					<img class="b-lazy services__icon" src="./assets/img/icons/14forty-icon-Business.svg" data-src="./assets/img/icons/14forty-icon-Business.svg" alt="">
					<p class="services__text">Food and hospitality</p>
				</div>
			</li>
			<li class="services">
				<div class="services__inner">
					<img class="b-lazy services__icon" src="./assets/img/icons/14forty-icon-Security.svg" data-src="./assets/img/icons/14forty-icon-Security.svg" alt="">
					<p class="services__text">Mechanical and Electrical</p>
				</div>
			</li>
		</ul>
	</div>
</section>