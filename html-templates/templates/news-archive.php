<section class="news--archive--block">

	<div class="news--archive--title">
		<div class="section__constrained center">
			<!-- TODO: Replace content of the following h1 with Page title field -->
			<h1 class="h2" data-aos="fade-left" data-aos-duration="500">Latest News</h1>
		</div>
	</div>


	<div class="latest-news wrapper__padding bottom">
		<div class="section__constrained">
			<div class="negative--margin">
				<div class="pure-g featured--news">

					<!-- TODO: Create a variable that will increase 100 for each loop iteration starting fron 0 and add this value to attribute data-aos-delay. Below php implementation for guidance -->

					<!--
					php incremental example, check end of loop:
					$delay = 0;
					for ($i = 0; $i < 3; $i++) {
					-->

					<?php
					$delay = 0;
					for ($i = 0; $i < 2; $i++) { ?>

					<!-- Repeater code for Featured news -->
					<div class="pure-u-1 pure-u-lg-1-2" data-aos="fade-up"
						 data-aos-delay="<?php echo $delay; ?>">
						<div class="news-cta">

							<!-- TODO: Replace data-src attribute with the article image field -->
							<div class="inner--image b-lazy mask" data-src="https://loremflickr.com/1200/900/hotel">
								<div class="content--wrapper">
									<div>

										<!-- TODO: Replace content of the following i with article date -->
										<i data-aos="fade-left" data-aos-delay="100" data-aos-duration="500">20 May 2019</i>

										<!-- TODO: Repalce href and content of a tag with Article URL adn content with Article Title field. Not render the div wrapper if button is empty -->
										<h2 data-aos="fade-left" data-aos-delay="150" data-aos-duration="500">
											<a href="/">placeholder text header copy to sit here</a>
										</h2>

										<!-- TODO: Replace p tag content with the article intro text field -->
										<p data-aos="fade-left" data-aos-delay="200" data-aos-duration="500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											qui.</p>
									</div>

									<div class="overlay" data-aos="fade-up" data-aos-duration="500"></div>
								</div>
							</div>
						</div>
					</div>
					<!--
					php incremental example, end of loop:
					$delay = $delay + 100;
					-->

					<!-- End of repeater code -->

					<?php
					$delay = $delay + 100;
					} ?>

				</div>

				<!-- Rest of articles - one third width -->
				<div class="pure-g list--news">

					<!-- TODO: Create a variable that will increase 100 for each loop iteration starting fron 0 and add this value to attribute data-aos-delay. Below php implementation for guidance -->

					<!--
					php incremental example, check end of loop:
					$delay = 0;
					for ($i = 0; $i < 3; $i++) {
					-->

					<?php
					$delay = 0;
					for ($i = 0; $i < 3; $i++) { ?>

					<!-- Repeater code -->

					<div class="pure-u-1 pure-u-lg-1-3" data-aos="fade-up"
						 data-aos-delay="<?php echo $delay; ?>">
						<div class="news-cta">

							<!-- TODO: Replace data-src attribute with the article image field -->
							<div class="inner--image b-lazy mask" data-src="https://loremflickr.com/1200/900/hotel">
								<div class="content--wrapper">
									<div>
										<!-- TODO: Replace content of the following i with article date -->
										<i data-aos="fade-left" data-aos-delay="100" data-aos-duration="500">19 May 2019</i>

										<!-- TODO: Repalce href and content of a tag with Article URL adn content with Article Title field. Not render the div wrapper if button is empty -->
										<h2 data-aos="fade-left" data-aos-delay="150" data-aos-duration="500">
											<a href="/">placeholder text header copy to sit here</a>
										</h2>
									</div>

									<div class="overlay" data-aos="fade" data-aos-duration="500"></div>
								</div>
							</div>
						</div>
					</div>

					<!--
					php incremental example, end of loop:
					$delay = $delay + 100;
					-->

					<!-- End of repeater code -->

						<?php
						$delay = $delay + 100;
					} ?>
				</div>

			</div>

			<!-- button to load more articles -->
			<div class="load-more-articles wrapper__flex center" data-aos="fade-up" data-aos-duration="500">
				<button type="submit" class="btn">load more</button>
			</div>
		</div>
	</div>
</section>
