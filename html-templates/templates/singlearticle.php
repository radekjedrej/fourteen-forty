<section class="single--article--block">

	<div class="featuredBanner__block">

		<div class="two-column heading">

			<div class="section__constrained">
				<div class="logo--wrapper"></div>
				<div>
					<div class="block--spacer"></div>

					<div class="content--wrapper">

						<!-- TODO: Replace content of the following div with Heading above title field. Not render this block if Image is empty -->
						<div data-aos="fade-left" data-aos-duration="500" data-aos-delay="50">
							<i>30 March 2019</i>
						</div>

						<!-- TODO: Replace content of the following div with Title field. Not render this block if Image is empty -->
						<h1 class="h2" data-aos="fade-left" data-aos-duration="500" data-aos-delay="100">Powerfull
							Partnerships</h1>

						<!-- TODO: Add content of field Overlay Colour after the class "background__". In this case light-blue is one of the values from the field -->
						<div class="overlay background__red opacity--80" data-aos="fade-up"
							 data-aos-duration="500"></div>

					</div>
				</div>

			</div>

		</div>

		<!-- TODO: Replace data-src attribute with the background URL field. Not render this block if Image is empty -->
		<div class="wrapper__parallax constrained" data-bottom-top="transform: translateY(50px)"
			 data-top-bottom="transform: translateY(-50px)">
			<div class="section__constrained border-box">
				<div class="wrapper__image b-lazy mask" data-src="https://loremflickr.com/1900/1000"></div>
			</div>
		</div>

	</div>

	<div class="background__white main--block" data-aos="fade-up" data-aos-delay="100" data-aos-duration="500">
		<div class="single--content">
			<div class="section__constrained">
				<div class="pure-g">
					<div class="pure-u-1 pure-u-lg-2-3" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
						<div class="heavy--content">
							<!-- TODO: Replace "main--text" content with Article body text field -->
							<div class="main--text">
								<h2>LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT, SED DO EIUSMOD TEMPOR INCID IDUNT UT LABORE ET DOLORE MAGNA ALIQUA.</h2>

								<h5>Sectors section</h5>
								<p>
									<a href="/">This is a link</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</p>

								<h3>Sectors section</h3>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</p>

								<h4>Sectors section</h4>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</p>

								<ul>
									<li>dfgdfg</li>
									<li>dfgfdgfdgdfgfdgd dgfdgdfgfd</li>
									<li>dfgdfgfd dgfdgfdgdfgfdgfdgdfgfdgdfgfd gdgfdgfdgfdgfdg</li>
									<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore mLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore mLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore mLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore mLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore m</li>
								</ul>

								<h5>Sectors section</h5>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</p>

							</div>
						</div>
					</div>

					<div class="pure-u-1 pure-u-lg-1-3" data-aos="fade-up" data-aos-delay="300" data-aos-duration="500">
						<div class="sidebar">
							<div class="inner">
								<h4 class="h2">Sidebar Title Here</h4>

								<a href="#">
									<div class="sidebar--item">
										<i>30 March 2019</i>
										<div class="inner">
											<h4 class="h2 item--title">blog post title text here</h4>
											<i class="icon-levy-right-arrow"> </i>
										</div>
									</div>
								</a>

								<a href="#">
									<div class="sidebar--item">
										<i>30 March 2019</i>
										<div class="inner">
											<h4 class="h2 item--title">blog post title text here</h4>
											<i class="icon-levy-right-arrow"> </i>
										</div>
									</div>
								</a>

								<a href="#">
									<div class="sidebar--item">
										<i>30 March 2019</i>
										<div class="inner">
											<h4 class="h2 item--title">blog post title text here</h4>
											<i class="icon-levy-right-arrow"> </i>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>

				</div>

				<div class="social--links--block">
					<div class="inner">
						<h3 class="h2">follow us and stay up to date</h3>
						<div class="levy__social">
							<ul>
								<li class="social-item">
									<a href="/">
										<i class="icon-facebook"></i>
									</a>
								</li>
								<li class="social-item">
									<a href="/">
										<i class="icon-twitter"></i>
									</a>
								</li>
								<li class="social-item">
									<a href="/">
										<i class="icon-linkedin"></i>
									</a>
								</li>
								<li class="social-item">
									<a href="/">
										<i class="icon-instagram"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>