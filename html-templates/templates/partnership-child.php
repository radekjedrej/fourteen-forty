<section class="featuredBanner__block">

	<div class="two-column heading partnerships--child">

		<div class="section__constrained">
			<div class="logo--wrapper">

				<!-- TODO: Replace data-src and src attributes with the Logo Overlay URL field. Not render this block if Image is empty -->
				<div class="inner--logo">
					<img class="b-lazy opacity" src="/assets-levy/img/logos/legendary-experiences-white.svg"
						 data-src="/assets-levy/img/logos/legendary-experiences-white.svg">
				</div>
			</div>
			<div class="partnerships--child--content">

				<div class="content--wrapper">
					<div data-aos="fade-left" data-aos-duration="500" data-aos-delay="50">
						<i>Partnerships</i>
					</div>

					<!-- TODO: Replace content of the following div with the child Partnership title.  -->
					<h1 class="h2" data-aos="fade-left" data-aos-duration="500" data-aos-delay="100">Stadia</h1>

					<!-- TODO: Add content of field Overlay Colour after the class "background__". In this case light-blue is one of the values from the field -->
					<div class="overlay background__light-blue opacity--80" data-aos="fade-up"
						 data-aos-duration="500"></div>

				</div>
			</div>

		</div>

	</div>

	<!-- TODO: Replace data-src attribute with the background image URL field. Not render this block if Image is empty -->
	<div class="wrapper__parallax constrained" data-bottom-top="transform: translateY(50px)"
		 data-top-bottom="transform: translateY(-50px)">
		<div class="section__constrained border-box">
			<div class="wrapper__image b-lazy mask" data-src="https://loremflickr.com/1900/1000"></div>
		</div>
	</div>

</section>

<!-- TODO: Replace content of p tag with Intro copy field. Not render this block if Image is empty -->
<section class="intro__text">
	<div class="section__constrained">
		<div class="center wrapper__max width--1000" data-aos="fade-left" data-aos-duration="500" data-aos-delay="150">
			<p class="h2">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT, SED DO EIUSMOD TEMPOR INCIDIDUNT UT
				LABORE ET DOLORE MAGNA ALIQUA.t</p>
		</div>
	</div>
</section>

<section class="links__block partnerships--archive background__transparent">

	<div class="wrapper__padding">
		<div class="section__constrained center">
			<div class="negative--margin">

				<div class="pure-g">

					<!-- TODO: Render all partnerships inside the current child -->
					<!-- TODO: Create a variable that will increase 100 for each loop iteration starting fron 0 and add this value to attribute data-aos-delay. Below php implementation for guidance -->

					<!--
					php incremental example, check end of loop:
					$delay = 0;
					for ($i = 0; $i < 3; $i++) {
					-->

					<?php
					$delay = 0;
					for ($i = 0; $i < 7; $i++) { ?>

						<!-- Repeater code -->

						<div class="pure-u-1 pure-u-md-1-2 pure-u-xl-1-3" data-aos="fade-up"
							 data-aos-delay="<?php echo $delay; ?>">
							<div class="news-cta">

								<!-- TODO: Replace data-src attribute with partnership image field -->
								<div class="shape cta inner--image b-lazy mask" data-src="https://loremflickr.com/1200/900/hotel">
									<div class="content--wrapper">
										<div>

											<!-- TODO: Repalce href and content of a tag with the single partnership link-->
											<h3 class="h2" data-aos="fade-left" data-aos-delay="150"
												data-aos-duration="500">
												<a href="/bingo">Partnership title</a>
											</h3>
										</div>

										<!-- TODO: Add random value between (red, green, yellow, dark-blue, light-blue, violet) after the class "background__". In this case dark-blue is one of the values from the field -->
										<div class="overlay background__dark-blue opacity--80" data-aos="fade"
											 data-aos-duration="500"></div>
									</div>
								</div>
							</div>
						</div>

						<!--
						php incremental example, end of loop:
						$delay = $delay + 100;
						-->

						<!-- End of repeater code -->

						<?php
						$delay = $delay + 100;
					} ?>
				</div>

			</div>
		</div>
	</div>

	<div class="relative--background">
		<div class="inner--background background__white"></div>
	</div>

</section>