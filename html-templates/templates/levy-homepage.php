<section class="homepage--header">
	<div class="section__constrained">
		<div class="negative--margin">
			<div class="pure-g">

				<!-- CTA primary block-->
				<div class="pure-u-1 pure-u-lg-2-3">

					<!-- TODO: Replace href of a tag with CTA Link field URL -->
					<a href="/">
						<!-- TODO: Replace data-src attribute with the Background image URL field. -->
						<div class="single--cta b-lazy mask" data-src="https://loremflickr.com/1200/900">
							<div class="content--wrapper">
								<div>
									<!-- Hardcoded image. Not necessary to be dynamic -->
									<img class="b-lazy opacity"
										 src="/assets-levy/img/logos/legendary-experiences-white.svg"
										 data-src="/assets-levy/img/logos/legendary-experiences-white.svg">

									<!-- TODO: Replace p tag content with Body Copy field. Not render if the field is empty -->
									<p data-aos="fade-left" data-aos-delay="350" data-aos-duration="500">At the game, at
										a gig or at a conference our promise remains the same.</p>
								</div>
								<!-- TODO: Add content of field Overlay Colour after the class "background__". In this case red is one of the values from the field -->
								<div class="overlay background__red opacity--80" data-aos="fade-up"
									 data-aos-duration="500"></div>
							</div>
						</div>
					</a>
				</div>

				<!-- CTA secondary repeater -->
				<div class="pure-u-1 pure-u-lg-1-3">

					<!-- Block of code for the repeater-->

					<!-- TODO: Replace data-src attribute with the Background image URL field. -->
					<div class="column--cta cta shape--a b-lazy mask" data-src="https://loremflickr.com/901/801">
						<div class="content--wrapper full--height">

							<div class="full--height">
								<div>
									<!-- TODO: Replace i tag content with Heading above title field. Not render if the field is empty -->
									<i data-aos="fade-left" data-aos-delay="300" data-aos-duration="500">Work for us</i>

									<!-- TODO: Replace href and content of a tag with CTA link field URL -->
									<a href="/" data-aos="fade-left" data-aos-delay="350" data-aos-duration="500">Key
										People</a>
								</div>
							</div>
							<!-- TODO: Add content of field Overlay Colour after the class "background__". In this case green is one of the values from the field -->
							<div class="overlay background__green opacity--80" data-aos="fade-up"
								 data-aos-duration="500"></div>

						</div>
					</div>

					<!-- Block of code for the repeater-->

					<!-- TODO: Replace data-src attribute with the Background image URL field. -->
					<div class="column--cta cta shape--a b-lazy mask" data-src="https://loremflickr.com/901/801">
						<div class="content--wrapper full--height">

							<div class="full--height">
								<div>
									<!-- TODO: Replace i tag content with Heading above title field. Not render if the field is empty -->
									<i data-aos="fade-left" data-aos-delay="300" data-aos-duration="500">Fantastic Food
										+ Drink</i>

									<!-- TODO: Replace href and content of a tag with CTA link field URL -->
									<a href="/" data-aos="fade-left" data-aos-delay="350" data-aos-duration="500">Services</a>
								</div>
							</div>


							<!-- TODO: Add content of field Overlay Colour after the class "background__". In this case green is one of the values from the field -->
							<div class="overlay background__violet opacity--80" data-aos="fade-up"
								 data-aos-duration="500"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Two columns block -->
<section class="content50__block home reverse">

	<div class="section__constrained">
		<div class="wrapper__relative">
			<div class="pure-g">
				<div class="pure-u-1 pure-u-lg-1-2">

				</div>
				<div class="pure-u-1 pure-u-lg-1-2">
					<div class="content__container wrapper__padding">

						<!-- TODO: Repalce h2 tag content with Bottom block title: field. Not render if the field is empty -->
						<h1 class="h2" data-aos="fade-left" data-aos-delay="50" data-aos-duration="500">BRAND STATEMENT,
							INTRODUCTION TEXT TO SHOUT ABOUT THE ETHOS</h1>

						<!-- TODO: Repalce "regular--content" content with Bottom block body copy field. Not render if the field is empty -->
						<div class="regular--content wrapper__text" data-aos="fade-left" data-aos-delay="150"
							 data-aos-duration="500">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
								incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui.
							</p>
						</div>

						<!-- TODO: Repalce href and content of a tag with button field. Not render the div wrapper if button is empty -->
						<div data-aos="fade-left" data-aos-delay="200" data-aos-duration="500" class="button--wrapper">
							<a href="#" class="btn">Call to action</a>
						</div>

					</div>
				</div>
			</div>

			<!-- TODO: Replace data-src attribute of inner--image with the Bottom block image field. Not render this block if Image is empty -->
			<div class="wrapper__image">
				<div class="b-lazy mask inner--image" data-src="https://loremflickr.com/900/800"></div>

				<!-- TODO: Replace data-src attribute of inner--logo with the Bottom block logo overlay field. Not render this block if Image is empty -->
				<div class="b-lazy opacity inner--logo" data-src="http://levy2019.compass.fanaticdev.co.uk/media/1060/good-times-white.svg"></div>
			</div>
		</div>
	</div>
</section>

<div class="background__white">
	<hr class="separator">
</div>
