<section class="featuredBanner__block">

	<div class="two-column heading">

		<div class="section__constrained">
			<div class="logo--wrapper case--studies">

				<!-- TODO: Replace data-src and src attributes with the Logo Overlay URL field. Not render this block if Image is empty -->
				<div class="inner--logo">
					<img class="b-lazy opacity" src="/assets-levy/img/logos/legendary-experiences-white.svg" data-src="/assets-levy/img/logos/legendary-experiences-white.svg">
				</div>
			</div>
			<div class="case--studies--details">
				<div class="block--spacer--case"></div>

				<div class="content--wrapper">

					<!-- TODO: Replace i tag content with Case Studies category name. -->
					<div data-aos="fade-left" data-aos-duration="500" data-aos-delay="50">
						<i>Stadia</i>
					</div>

					<!-- TODO: Replace h1 tag content with the Case Studies Title field -->
					<h1 class="h2" data-aos="fade-left" data-aos-duration="500" data-aos-delay="100">TOTTENHAM HOTSPUR</h1>

					<div class="overlay background__dark-blue opacity--80" data-aos="fade-up" data-aos-duration="500"></div>

					<table>
						<tr data-aos="fade-left" data-aos-duration="500" data-aos-delay="150">
							<td class="spacer">
								<strong>Partner since</strong>
							</td>

							<!-- TODO: Replace td tag content with the Case Studies Partner Since field -->
							<td>
								2016
							</td>
						</tr>
						<tr data-aos="fade-left" data-aos-duration="500" data-aos-delay="200">
							<td class="spacer">
								<strong>Deliverables</strong>
							</td>
							<td>
								<ul>
									<!-- TODO: Replace li tag content with the Case Studies Deliverables field -->
									<!-- Code for the loop-->
									<li>Restaurant</li>
									<!-- End Code for the loop -->
								</ul>
							</td>
						</tr>
					</table>

				</div>
			</div>

		</div>

	</div>

	<!-- TODO: Replace data-src attribute with the background URL field. Not render this block if Image is empty -->
	<div class="wrapper__parallax constrained" data-bottom-top="transform: translateY(50px)" data-top-bottom="transform: translateY(-50px)" >
		<div class="section__constrained border-box">
			<div class="wrapper__image b-lazy mask" data-src="https://loremflickr.com/1900/1000"></div>
		</div>
	</div>

</section>

<!-- TODO: Replace content of p tag with Intro copy field. Not render this block if Image is empty -->
<section class="intro__text">
	<div class="section__constrained">
		<div class="center wrapper__max width--1000" data-aos="fade-left" data-aos-duration="500" data-aos-delay="150">
			<p class="h2">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT, SED DO EIUSMOD TEMPOR INCIDIDUNT UT LABORE ET DOLORE MAGNA ALIQUA.t</p>
		</div>
	</div>
</section>

<!-- TODO: Not render if Case Studies Stats field is empty -->
<section class="caseStudiesStats__block  background__white">
	<div class="wrapper__padding double">
		<div class="section__constrained center">
			<div class="pure-g">
				<!-- Code for the loop-->
				<div class="pure-u-1 pure-u-md-1-2 pure-u-lg-1-3">
					<div class="single--stat">
						<div>
							<!-- TODO: Replace i tag content with the Case Studies First text line field -->
							<i>Delivering</i>

							<!-- TODO: Replace p tag content with the Case Studies Value field -->
							<p>333</p>

							<!-- TODO: Replace i tag content with the Case Studies Second text line: field -->
							<i>Racedays</i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>